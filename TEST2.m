clc;
clear;
close all;
%%load image
img1=imread('pout.tif');
npixel=numel(img1);
sigma=30;
mu=50;

H=@(x)exp(-1/(2*sigma^2)*(x-mu).^2);
x=0:255;
hgram=H(x);
hgram=round(npixel*hgram/sum(hgram));
% plot(x,hgram);
stem(x,hgram);
img2=histeq(img1,hgram);
img5=adapthisteq(img1);
img6=adapthisteq(img1,...
    'Distribution','exponential',...
    'Alpha',0.1,...
    'NumTiles',[3 3]);
% img1=imread('pout.tif');
% img2=imadjust(img1);
% %img3=imadjust(img1,stretchlim(img1),[0.2 1]);
% %img4=imadjust(img1,[0.2902 0.8784],[0.9 1]);
% img3=imadjust(img1,stretchlim(img1),[0 1],0.1);%%5%���� ���� ���� ��� �?���
% %img4=imadjust(img1,[0.2902 0.8784],[0.4 0.6],2);
% img4 =histeq(img1);
figure;
subplot(2,2,1);
% imshow(img1);
imhist(img5,256);
title('orginal image');
subplot(2,2,2);
% imshow(img2);
% imhist(img2,256);
imhist(img6,256);
% title('intencity image');
% subplot(2,2,3);
% %imshow(img3);
% imhist(img3,256);
% title('prrety indsity  image');
% subplot(2,2,4);
% %imshow(img4);
% imhist(img4,256);
% title('prrety 2indsity  image');